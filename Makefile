filename=main

all: $(filename).pdf

generate: chemicals.list
	./generate.sh "$<" ./mol/structures.tex
	./genflash.sh "$<"
	
%.pdf: %.tex
	pdflatex $<

clean:
	rm -f *.log *.aux *.bbl *.blg *.out *.fls *.nav *.snm *.nav *.toc *.fdb_latexmk *.synctex.gz *.dvi _minted-*

publish:
	cp $(filename).pdf ../